"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.duplicationArr = void 0;
const duplicationArr = (arr, key) => {
    let newArray;
    if (key) {
        newArray = arr.reduce((pre, cur) => pre.some((el) => el[key] === cur[key]) ? pre : [...pre, cur], []);
    }
    else {
        newArray = arr.reduce((pre, cur) => {
            const isEixt = pre.find((el) => el === cur);
            if (!isEixt) {
                pre = [...pre, cur];
            }
            return pre;
        }, []);
    }
    return newArray;
};
exports.duplicationArr = duplicationArr;
//# sourceMappingURL=index.js.map