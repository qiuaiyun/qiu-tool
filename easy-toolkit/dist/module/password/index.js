"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePassword = void 0;
/**
 * At least 8 characters long, including numbers,
 * uppercase and lowercase letters,
 * and special characters
 */
const powerful = {
    level: "powerful",
    reg: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*.-_]).{8,}$/
};
/**
 * At least 8 characters long, including numbers and uppercase and lowercase letters
 */
const middle = {
    level: "middle",
    reg: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/
};
/**
 * At least 8 bits in length
 */
const weak = {
    level: "weak",
    reg: /^.{8,}$/
};
/**
 * Password verification
 *
 * @param { string } password value
 * @param { string } password level "weak"|"middle"|"powerful"
 * @returns { boolean } returns boolean value
 */
const validatePassword = (password, level) => {
    const levels = level ? level : 'powerful';
    switch (levels) {
        case "powerful":
            return powerful.reg.test(password);
        case "middle":
            return middle.reg.test(password);
        case "weak":
            return weak.reg.test(password);
        default:
            return `not spport ${levels} mode.`;
    }
};
exports.validatePassword = validatePassword;
//# sourceMappingURL=index.js.map