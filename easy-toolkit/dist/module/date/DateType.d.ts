export type DateFormat = "YYYY" | "YYYY-MM" | "YYYY-MM-DD" | "YYYY/MM/DD" | "YYYY-MM-DD HH" | "YYYY/MM/DD HH" | "YYYY-MM-DD HH:MM" | "YYYY/MM/DD HH:MM" | "YYYY-MM-DD HH:MM:SS" | "YYYY/MM/DD HH:MM:SS" | "HH:MM:SS";
export type DateOptions = {
    format?: DateFormat;
    date?: Date | String;
};
