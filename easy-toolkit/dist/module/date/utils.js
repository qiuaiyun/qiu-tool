"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isTimeValid = exports.addZero = exports.setZhCnTimeStr = void 0;
const setZhCnTimeStr = (value_, time_) => {
    var dateStr = "";
    if (!(0, exports.isTimeValid)(time_)) {
        return dateStr = `${time_} Is Invalid Date.`;
    }
    const date = time_ ? typeof time_ === 'string' ? new Date(time_) : new Date() : new Date();
    const year = date.getFullYear();
    const month = (0, exports.addZero)(date.getMonth() + 1);
    const day = (0, exports.addZero)(date.getDate());
    const hour = (0, exports.addZero)(date.getHours());
    const minute = (0, exports.addZero)(date.getMinutes());
    const second = (0, exports.addZero)(date.getSeconds());
    switch (value_) {
        case 'YYYY':
            dateStr = `${year}年`;
            break;
        case 'YYYY-MM':
            dateStr = `${year}年${month}月`;
            break;
        case 'YYYY-MM-DD':
            dateStr = `${year}年${month}月${day}日`;
            break;
        case 'YYYY-MM-DD HH':
            dateStr = `${year}年${month}月${day}日 ${hour}`;
            break;
        case 'YYYY-MM-DD HH:MM':
            dateStr = `${year}年${month}月${day}日 ${hour}点${minute}分`;
            break;
        case 'HH:MM:SS':
            dateStr = `${hour}点${minute}分${second}秒`;
            break;
        case 'YYYY-MM-DD HH:MM:SS':
            dateStr = `${year}年${month}月${day}日 ${hour}点${minute}分${second}秒`;
            break;
        default:
            dateStr = `${year}年${month}月${day}日 ${hour}点${minute}分${second}秒`;
            break;
    }
    return dateStr;
};
exports.setZhCnTimeStr = setZhCnTimeStr;
const addZero = (num) => {
    return +num > 9 ? num : `0${num}`;
};
exports.addZero = addZero;
const isTimeValid = (time_) => {
    let flag = true;
    if (time_ && Object.prototype.toString.call(time_) === '[object String]') {
        const times = new Date(time_);
        if (isNaN(times.getTime())) {
            flag = false;
        }
    }
    else if (time_ && Object.prototype.toString.call(time_) === '[object Date]') {
        const timestamps = new Date(time_);
        if (isNaN(timestamps.getTime())) {
            flag = false;
        }
    }
    else if (time_ === void 0) {
        flag = true;
    }
    return flag;
};
exports.isTimeValid = isTimeValid;
//# sourceMappingURL=utils.js.map