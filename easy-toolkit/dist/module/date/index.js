"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateKit = exports.dateKit = void 0;
const weekDays_1 = require("./weekDays");
const utils_1 = require("./utils");
/**
 * DateKit class
 *
 * @author qiuny
 */
const SEPARATE_TIME = ':';
let C_Date = new Date();
let C_Year = C_Date.getFullYear();
let C_Month = C_Date.getMonth() + 1;
let C_Day = C_Date.getDate();
let C_Hour = C_Date.getHours();
let C_Minute = C_Date.getMinutes();
let C_Second = C_Date.getSeconds();
class DateKit {
    constructor(_date) {
        // this.isTrue = _date ? true : false;
        // this.initDate(_date)
    }
    /**
     *  Set date by developer
     * @param _date YYYY-MM-DD HH:MM:SS or 2023-09-01T03:17:45.944Z
     * @returns date
     */
    static setDate(_date) {
        // this.isTrue = _date ? true : false;
        // this.initDate(_date)
        C_Date = _date ? new Date(_date) : null;
        C_Year = C_Date.getFullYear();
        C_Month = C_Date.getMonth() + 1;
        C_Day = C_Date.getDate();
        C_Hour = C_Date.getHours();
        C_Minute = C_Date.getMinutes();
        C_Second = C_Date.getSeconds();
        return this;
    }
    // Get timestamp
    static getTimeStamp() {
        return C_Date.getTime();
    }
    // Get the total number of days and list of the current month.
    static getTotalDays() {
        const lastDay = new Date(C_Year, C_Month, 0).getDate();
        let totalDay = [];
        for (let i = 0; i < lastDay; i++) {
            totalDay = totalDay.concat([i + 1]);
        }
        return totalDay;
    }
    /**
     * Get what day of the week today is
     * @param _date pattern: YYYY-MM-DD HH:MM:SS
     * @returns week
     */
    static getWeekOfDate(_date) {
        const dates = _date ? new Date(_date) : C_Date;
        return weekDays_1.weekDays[dates.getDay()];
    }
    // Get date or coversion date by format
    static format(format) {
        let currentDate = "";
        switch (format) {
            case "YYYY":
                currentDate = `${C_Year}`;
                break;
            case "YYYY-MM":
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}`;
                break;
            case "YYYY-MM-DD":
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}-${(0, utils_1.addZero)(C_Day)}`;
                break;
            case "YYYY/MM/DD":
                currentDate = `${C_Year}/${(0, utils_1.addZero)(C_Month)}/${(0, utils_1.addZero)(C_Day)}`;
                break;
            case "YYYY-MM-DD HH":
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}-${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}`;
                break;
            case "YYYY/MM/DD HH":
                currentDate = `${C_Year}/${(0, utils_1.addZero)(C_Month)}/${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}`;
                break;
            case "YYYY-MM-DD HH:MM":
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}-${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}`;
                break;
            case "YYYY/MM/DD HH:MM":
                currentDate = `${C_Year}/${(0, utils_1.addZero)(C_Month)}/${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}`;
                break;
            case "YYYY-MM-DD HH:MM:SS":
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}-${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Second)}`;
                break;
            case "YYYY/MM/DD HH:MM:SS":
                currentDate = `${C_Year}/${(0, utils_1.addZero)(C_Month)}/${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Second)}`;
                break;
            case "HH:MM:SS":
                currentDate = `${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Second)}`;
                break;
            default:
                currentDate = `${C_Year}-${(0, utils_1.addZero)(C_Month)}-${(0, utils_1.addZero)(C_Day)} ${(0, utils_1.addZero)(C_Hour)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Minute)}${SEPARATE_TIME}${(0, utils_1.addZero)(C_Second)}`;
                break;
        }
        return currentDate;
    }
    /**
     * Get historical time
     * format: YYYY-MM-DD HH:MM:SS
     *         | YYYY
     *         | YYYY-MM
     *         | YYYY-MM-DD HH
     *         | YYYY-MM-DD HH:MM
     * @param { timestamps: string } date of value
     * @returns object{ time: xxx, unit: 'day', text: 'minutes ago'}
     */
    static getPastTime(timestamps) {
        const dateEnd = new Date();
        const dateBegin = new Date(timestamps);
        const dateDiff = dateEnd.getTime() - dateBegin.getTime();
        const seconds = Math.floor(dateDiff / 1000);
        const minutes = Math.floor(seconds / 60);
        const hours = Math.floor(minutes / 60);
        const days = Math.floor(hours / 24);
        const years = Math.floor(days / 365);
        if (years > 0) {
            return { time: years, unit: 'year', text: 'year ago' };
        }
        else if (days > 0) {
            return { time: days, unit: 'day', text: 'day ago' };
        }
        else if (hours > 0) {
            return { time: hours, unit: 'hour', text: 'hour ago' };
        }
        else if (minutes > 0) {
            return { time: minutes, unit: 'minute', text: 'minutes ago' };
        }
        else {
            return { time: seconds, unit: 'second', text: 'second ago' };
        }
    }
    // Add missing 0
    static addZero(num) {
        return +num > 9 ? `${num}` : `0${num}`;
    }
    static zhCnTime(format, date) {
        var zhCnTimes = (0, utils_1.setZhCnTimeStr)(format, date);
        return zhCnTimes;
    }
}
exports.DateKit = DateKit;
/**
 *  Calculate the difference between time intervals and return the difference in days, hours and minutes
 *
 * @param {*} startTime Starting time
 * @param {*} endTime End Time
 * @returns
 * {
 *    day: days,
 *    hours: hour,
 *    timeStr: time,
 *    minutes
 *  }
 */
DateKit.calcDateDifference = (startTime, endTime) => {
    const _s = new Date(startTime).toString();
    const _e = new Date(endTime).toString();
    if (_s === 'Invalid Date' || _e === 'Invalid Date') {
        throw new Error('Invalid Date');
    }
    const _start_time_stamp = new Date(startTime).getTime();
    const _end_time_stamp = new Date(endTime).getTime();
    // Number of milliseconds difference between two timestamps.
    const _used_time = _end_time_stamp - _start_time_stamp;
    // Days difference.
    const _days_value = Math.floor(_used_time / (24 * 3600 * 1000));
    // Calculate hours.
    const _hour_value = _used_time % (24 * 3600 * 1000);
    // Keep all decimals and round off when returning to ensure the accuracy of the data.
    const _hours = _hour_value / (3600 * 1000);
    // Calculate difference in minutes.
    const _hour_value_minute = _hour_value % (3600 * 1000);
    // Calculate the number of milliseconds remaining after hours.
    const minutes = Math.floor(_hour_value_minute / (60 * 1000));
    const hour = +_hours.toFixed(2);
    const time = `${_days_value}天${hour}时${minutes}分钟`;
    return {
        day: _days_value,
        hours: hour,
        timeStr: time,
        minutes
    };
};
const dateKit = DateKit;
exports.dateKit = dateKit;
//# sourceMappingURL=index.js.map