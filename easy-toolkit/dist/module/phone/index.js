"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePhone = void 0;
/**
 * Phone number verification
 *
 * @author qiuny
 * @param { string | number } telephone
 * @returns { Boolean } return boolean
 */
const phoneReg = /^1(3[0-9]|4[5-9]|5[0-35-9]|6[56]|7[0-8]|8[0-9]|9[89])\d{8}$|^1(3[0-2]|4[5-6]|5[56]|6[67]|7[156]|8[56])\d{8}$|^1(3[34]|4[9]|5[37]|6[478]|7[3478]|8[019]|9[0139])\d{8}$/;
const validatePhone = (telephone) => {
    let values = '';
    if (typeof telephone === 'number') {
        values = telephone.toString();
    }
    else {
        values = telephone;
    }
    return phoneReg.test(values);
};
exports.validatePhone = validatePhone;
//# sourceMappingURL=index.js.map