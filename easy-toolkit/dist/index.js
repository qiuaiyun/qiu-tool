"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupByKey = exports.dateKit = exports.DateKit = exports.weekDays = exports.duplicationArr = exports.deepClone = exports.validateEmail = exports.validatePhone = exports.validatePassword = exports.getUUID = exports.Shuffle = void 0;
const email_1 = require("./module/email");
const date_1 = require("./module/date");
const weekDays_1 = require("./module/date/weekDays");
const group_1 = require("./module/group");
const clone_1 = require("./module/clone");
const deduplication_1 = require("./module/deduplication");
const phone_1 = require("./module/phone");
const password_1 = require("./module/password");
const uuid_1 = require("./module/uuid");
const shuffle_1 = require("./module/shuffle");
/**
 * class Shuffle
 *
 * @description Get a random English name or Chinese name
 */
class Shuffle extends shuffle_1.Shuffle {
}
exports.Shuffle = Shuffle;
;
/**
 * Function
 *
 * @description Get unique uuid
 * @returns a 32-bit string
 */
const getUUID = () => (0, uuid_1.getUUID)();
exports.getUUID = getUUID;
/**
 * Function
 *
 * @description Password verification
 * @returns boolean|string
 */
const validatePassword = (password, level) => (0, password_1.validatePassword)(password, level);
exports.validatePassword = validatePassword;
/**
 * Function
 *
 * @description Phone number verification.
 *
 * Check telephone format is right by this method.
 *
 * @returns boolean
 */
const validatePhone = (telephone) => (0, phone_1.validatePhone)(telephone);
exports.validatePhone = validatePhone;
/**
 * Function
 *
 * @description Verify whether the email format is legal.
 * @returns boolean
 */
const validateEmail = (email) => (0, email_1.validateEmail)(email);
exports.validateEmail = validateEmail;
/**
 * Function
 *
 * @description Deep clone data, supporting objects, arrays, and strings.
 * @returns val
 */
const deepClone = (val) => (0, clone_1.deepClone)(val);
exports.deepClone = deepClone;
/**
 * Function
 *
 * @description Array deduplication
 * @param arr a array value
 * @param key unique key of object
 * @returns Array<T>
 */
const duplicationArr = (arr, key) => {
    return (0, deduplication_1.duplicationArr)(arr, key);
};
exports.duplicationArr = duplicationArr;
/**
 * class DateKit
 * dateKit instance of DateKit
 *
 * @description Time operation class
 */
exports.weekDays = weekDays_1.weekDays;
exports.DateKit = date_1.DateKit;
exports.dateKit = date_1.dateKit;
/**
 * Function
 *
 * @description Implement array data grouping method based on type
 * @param val An object
 * @returns Record<string, unknown[]>
 */
const groupByKey = (array, fn) => {
    return (0, group_1.groupByKey)(array);
};
exports.groupByKey = groupByKey;
exports.default = {
    groupByKey: exports.groupByKey
};
//# sourceMappingURL=index.js.map