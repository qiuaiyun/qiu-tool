import { surnamesData, Names } from "./surnames";
/**
 * Get random names
 * 
 * @author qiuny
 */
class Shuffle {
    /**
     * Get a random English name
     * 
     * @param count Control quantity parameters
     */
    static englishNames(count?: number | string): Array<string>;

    static englishNames(count?: number | string): string | Array<string> {
        let tempArr:Array<string> = [];
        if (typeof Number(count) === "number" && Number(count) > 0) {
            for (let i = 0; i < Number(count); i++) {
                tempArr = tempArr.concat([Shuffle.randomEnName()])
            }
            return tempArr.map(mp => mp.charAt(0).toUpperCase() + mp.slice(1));
        }
        const strName = Shuffle.randomEnName()
        return strName.charAt(0).toUpperCase() + strName.slice(1)
    }
    
    static randomEnName () {
        const vowels:string = "aeiou";
        const consonants:string = "bcdfghjklmnpqrstvwxyz";
        const nameLength:number = Math.floor(Math.random() * 4) + 5;
        let tempName:string = "";
        for (let i = 0; i < nameLength; i++) {
            if (i % 2 === 0) {
                tempName += vowels.charAt(Math.floor(Math.random() * vowels.length));
            } else {
                tempName += consonants.charAt(Math.floor(Math.random() * consonants.length));
            }
        }
        return tempName;
    }

    /**
     * Get a random Chinese name
     * 
     * @param count Control quantity parameters
     */
    static chineseNames(count?: number | string): Array<string>;

    static chineseNames(count?: number | string): string | Array<string> {
        const randomSurname = Math.floor(Math.random() * surnamesData.length)
        const randomName = Math.floor(Math.random() * Names.length)
        if (typeof Number(count) === 'number' && Number(count) > 0) {
            let tmepArr: Array<string> = []
            for (let i = 0; i < Number(count); i++) {
                const SurnameIndex = Math.floor(Math.random() * surnamesData.length)
                const NameIndex = Math.floor(Math.random() * Names.length)
                tmepArr = tmepArr.concat([`${surnamesData[SurnameIndex]}${Names[NameIndex]}`])
            }
            return tmepArr
        }
        return `${surnamesData[randomSurname]}${Names[randomName]}`
    }
}

export {
    Shuffle
}
