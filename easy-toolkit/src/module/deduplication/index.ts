/**
 * Array deduplication
 * 
 * @author qiuny
 */
type ArrayType = {
    [key:string]: any
}
export const duplicationArr = <T, K extends keyof T>(arr: T[], key?: K):T[] => {
    let newArray:T[];
    if (key) {
        newArray = arr.reduce((pre:T[], cur:T) => pre.some((el:T) => el[key] === cur[key]) ? pre : [...pre, cur], [])
    } else {
        newArray = arr.reduce((pre:T[], cur:T) => {
            const isEixt = pre.find((el: T) => el === cur)
            if (!isEixt) {
                pre = [...pre, cur]
            }
            return pre;
        }, [])
    }
    return newArray;
}