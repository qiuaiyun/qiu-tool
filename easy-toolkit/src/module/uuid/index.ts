/**
 * Get unique id
 * 
 * @author qiuny
 * @returns {string}
 */
type Char = '-' | '_' | '*' | '#' | '&'
const getUUID = (char?: Char) => {
    let d = new Date().getTime();
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    if (char) return uuid.replace(/-/g, char)
    return uuid.replace(/-/g, '');
}

export {
    getUUID
}