
interface Regx {
    readonly reg: RegExp
}
interface Powerful extends Regx {
    level: 'powerful',
}
interface Middle extends Regx {
    level: 'middle'
}
interface Weak extends Regx {
    level: 'weak'
}

type Level = Powerful | Middle | Weak;

/**
 * At least 8 characters long, including numbers, 
 * uppercase and lowercase letters,
 * and special characters
 */
const powerful: Level = {
    level: "powerful",
    reg: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*.-_]).{8,}$/
}
/**
 * At least 8 characters long, including numbers and uppercase and lowercase letters
 */
const middle: Level = {
    level: "middle",
    reg: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/
}

/**
 * At least 8 bits in length
 */
const weak: Level = {
    level: "weak",
    reg: /^.{8,}$/
}

export type Options = 
|"weak"
|"middle"
|"powerful"

/**
 * Password verification
 * 
 * @param { string } password value
 * @param { string } password level "weak"|"middle"|"powerful"
 * @returns { boolean } returns boolean value
 */
const validatePassword = (password: string, level?: Options): boolean|string => {
    const levels:Options = level ? level : 'powerful';
    switch (levels) {
        case "powerful":
            return powerful.reg.test(password);
        case "middle":
            return middle.reg.test(password);
        case "weak":
            return weak.reg.test(password);
        default:
            return `not spport ${levels} mode.`;
    }
}
export {
    validatePassword,
}