## 安装：
npm install --save-dev jest

## vscode配置：
launch.json
```js
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Jest Test",
            "type": "node",
            "request": "launch",
            "program": "${workspaceFolder}/node_modules/jest/bin/jest",
            "args": [
                "--runInBand"
            ],
            "cwd": "${workspaceFolder}",
            "console": "integratedTerminal",
            "internalConsoleOptions": "neverOpen",
            "disableOptimisticBPs": true
        }
    ]
}
```

## 新建test文件夹或者__tests__文件夹
文件命名：xxx.test.ts

```js
import { greet } from "../src/module/greet";
describe('greet', () => {
    test('执行结果如下：', () => {
        const res = greet('qiunanya')
        // expect(res).toBe("hello qiunanya")
        expect(res).toEqual("hello qiunanya")
    })
})
```

## 运行左侧菜单小虫子
方法有断点就会停留断点，否则继续执行，直到函数是否符合预期结果

## 配置package.json 
jest默认测试 `所有` 以xxx.test.ts或xxx.spec.ts格式的文件。
```js
# 配置
"test": "jest"
# 测试命令 
npm run test
```

## 测试指定文件
参考网址:[https://jestjs.io/zh-Hans/docs/cli](https://jestjs.io/zh-Hans/docs/cli)

### npm测试指定文件
这样运行命令，不需要依赖全局安装的jest，利用项目安装的jest即可。
```js
# 配置
"test": "jest"
# 命令
npm run test date.test.ts
```

npm 全局安装jest依赖
```js
npm install jest -g
```
### 仅运行指定文件名称或文件路径的测试

```js
# 指定测试文件的名称
jest xxx.test.ts|js|tsx|jsx
# 指定测试文件的路径
jest ./__tests__/date.test.ts
```

### 指定多文件测试
需要指定--findRelatedTests属性值

```js
jest --findRelatedTests date.test.ts phone.spec.ts
```