import { validateEmail } from "../src/module/email"

describe('greet', () => {
    test('邮箱测试', () => {
        const res = validateEmail('6666@qq.com')
        expect(res).toBe(true)
    })
    test('空邮箱测试', () => {
        const res = validateEmail()
        expect(res).toEqual({ error: 'email is not empty' })
    })
})