import { total } from "../src/module/sum";

describe('sum', () => {
    test('默认 0 加 0 等于 0', () => {
        const res = total()
        expect(res).toBe(0)
    })
    test('1 加 0 等于 1', () => {
        const res = total(1)
        expect(res).toBe(1)
    })
    test('1 加 6 等于 7', () => {
        const res = total(1, 6)
        expect(res).toBe(7)
    })
})