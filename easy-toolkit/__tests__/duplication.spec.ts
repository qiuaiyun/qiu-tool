import { duplicationArr } from "../src/module/deduplication";

describe('duplication', () => {
    const storehouse = [
        { name: "asparagus", type: "vegetables", quantity: 5 },
        { name: "bananas", type: "fruit", quantity: 0 },
        { name: "goat", type: "meat", quantity: 23 },
        { name: "cherries", type: "fruit", quantity: 5 },
        { name: "fish", type: "meat", quantity: 22 },
    ];
    const result = [
        { name: 'asparagus', type: 'vegetables', quantity: 5 },
        { name: 'bananas', type: 'fruit', quantity: 0 },       
        { name: 'goat', type: 'meat', quantity: 23 }
    ]
    const arr = [1,2,5,2,5,2,541,25,555]
    test('字符串数组去重', () => {
        const res = duplicationArr(arr)
        expect(res).toEqual([ 1, 2, 5, 541, 25, 555 ])
    })
    test('对象数组去重', () => {
        const res = duplicationArr(storehouse, 'type')
        expect(res).toEqual(result)
    })
})