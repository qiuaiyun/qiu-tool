import { validatePassword, Options } from "../src/module/password"

describe('validate phone', () => {
    test('测试空密码', () => {
        const res = validatePassword('')
        expect(res).toBe(false)
    })
    test('测试null', () => {
        const res = validatePassword('null')
        expect(res).toBe(false)
    })
    test('测试默认强密码模式', () => {
        const res = validatePassword('Abc123!@')
        expect(res).toBe(true)
    })
    test('测试弱密码', () => {
        const res = validatePassword('abc123789', 'weak')
        expect(res).toBe(true)
    })
    test('中等密码', () => {
        const res = validatePassword('Abc1234567', 'middle')
        expect(res).toBe(true)
    })

    test('强密码', () => {
        const res = validatePassword('Abc123!@', 'powerful')
        expect(res).toBe(true)
    })

    test('强密码1', () => {
        const res = validatePassword('Abc123!@13.0', 'powerful')
        expect(res).toBe(true)
    })

    test('强密码2', () => {
        const res = validatePassword('Abc123!@13.0_', 'powerful')
        expect(res).toBe(true)
    })

    test('其他等级，默认是高级密码校验', () => {
        const res = validatePassword('Abc123!@', 'other' as Options)
        expect(res).toBe('not spport other mode.')
    })
})