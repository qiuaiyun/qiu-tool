import { dateKit } from "../src/module/date";
describe('dateKit.format.', () => {
    test('获取当前中文时间', () => {
        const res = dateKit.zhCnTime()
        // expect(res).toEqual('2024年')
        console.log('获取当前中文时间:',res);
    })
    test('当前中文年', () => {
        // expect(res).toEqual('2024年') 会过期，每次测试需要额外调整时间
        const res = dateKit.zhCnTime('YYYY')
        // expect(res).toEqual('2024年')
        console.log('当前中文年:',res);
    })
    test('当前中文年月', () => {
        const res = dateKit.zhCnTime('YYYY-MM')
        // expect(res).toEqual('2024年09月')
        console.log('当前中文年:',res);
    })
    test('当前中文年月日', () => {
        const res = dateKit.zhCnTime('YYYY-MM-DD')
        // expect(res).toEqual('2024年09月13日')
        console.log('当前中文年月日:',res);
    })
    test('当前中文年月日时', () => {
        const res = dateKit.zhCnTime('YYYY-MM-DD HH')
        console.log('当前中文年月日时:',res);
    })
    test('当前中文年月日时分', () => {
        const res = dateKit.zhCnTime('YYYY-MM-DD HH:MM')
        console.log('当前中文年月日时分:',res);
    })
    test('当前中文年月日时分秒', () => {
        const res = dateKit.zhCnTime('YYYY-MM-DD HH:MM:SS')
        console.log('当前中文年月日时分秒:',res);
    })
    test('当前中文时分秒', () => {
        const res = dateKit.zhCnTime('HH:MM:SS')
        console.log('当前中文时分秒:',res);
    })
    test('传递时间字符串', () => {
        const res = dateKit.zhCnTime('HH:MM:SS', '2023-04-05 15:45:04')
        console.log('当前中文时分秒:',res);
    })
    test('传递时间对象', () => {
        const res = dateKit.zhCnTime('HH:MM:SS', new Date())
        console.log('当前中文时分秒:',res);
    })
    test('传递错误时间对象', () => {
        const res = dateKit.zhCnTime('HH:MM:SS', new Date('sdfsbdfsdf'))
        console.log('传递错误时间对象:',res);
    })
    test('传递错误时间字符串', () => {
        const res = dateKit.zhCnTime('HH:MM:SS', 'sdfsbdfsdf')
        console.log('传递错误时间字符串:',res);
    })


    test('获取当前月总天数', () => {
        const res = dateKit.getTotalDays()
        expect(res).toEqual(res)
    })
    test('失效时间测试', () => {
        try {
            dateKit.calcDateDifference('2023-00-00', '000-09-19')
        } catch (error) {
            expect(`${error}`).toEqual('Error: Invalid Date')
        }
    })
    test('获取时间差值', () => {
        const res = dateKit.calcDateDifference('2023-08-18', '2023-09-19')
        expect(res).toEqual({ day: 32, hours: 0, timeStr: '32天0时0分钟', minutes: 0 })
    })

    test('获取时间差值异常', () => {
        const res = dateKit.calcDateDifference('2023-08-18', '2023-09-19')
        expect(res).toEqual({ day: 32, hours: 0, timeStr: '32天0时0分钟', minutes: 0 })
    })

    test('时间字符串是星期几', () => {
        const res = dateKit.getWeekOfDate("2023-08-18 11:46:04")
        expect(res).toBe('星期五')
    })
    test('当前时间星期几', () => {
        const res = dateKit.getWeekOfDate()
        expect(res).toBe(res)
    })
    test('获取当前时间戳', () => {
        const res = dateKit.getTimeStamp()
        expect(res).toBe(res)
    })
    test('不传参数当前时间YYYY-MM-DD HH:MM:SS', () => {
        const res = dateKit.format()
        expect(res).toEqual(res)
    })
    test('当前时间YYYY-MM-DD', () => {
        const res = dateKit.format('YYYY-MM-DD')
        expect(res).toEqual(res)
    })
    test('当前时间YYYY/MM/DD', () => {
        const res = dateKit.format('YYYY/MM/DD')
        expect(res).toEqual(res)
    })
    test('当前时间YYYY-MM-DD HH', () => {
        const res = dateKit.format('YYYY-MM-DD HH')
        expect(res).toEqual(res)
    })
    test('当前时间YYYY/MM/DD HH', () => {
        const res = dateKit.format("YYYY/MM/DD HH")
        expect(res).toEqual(res)
    })
    test('当前时间YYYY-MM-DD HH:MM:SS', () => {
        const res = dateKit.format('YYYY-MM-DD HH:MM:SS')
        expect(res).toEqual(dateKit.format())
    })
    test('当前时间YYYY/MM/DD HH:MM:SS', () => {
        const res = dateKit.format("YYYY/MM/DD HH:MM:SS")
        expect(res).toEqual(dateKit.format("YYYY/MM/DD HH:MM:SS"))
    })
    test('当前时间YYYY-MM-DD HH:MM', () => {
        const res = dateKit.format("YYYY-MM-DD HH:MM")
        expect(res).toEqual(dateKit.format("YYYY-MM-DD HH:MM"))
    })
    test('当前时间YYYY/MM/DD HH:MM', () => {
        const res = dateKit.format("YYYY/MM/DD HH:MM")
        expect(res).toEqual(dateKit.format("YYYY/MM/DD HH:MM"))
    })
    test('获取当前年月日', () => {
        const res = dateKit.format('YYYY-MM-DD')
        expect(res).toEqual(res)
    })
    test('获取当前年月日 YYYY/MM/DD', () => {
        const res = dateKit.format('YYYY/MM/DD')
        expect(res).toEqual(res)
    })
    test('YYYY 格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format("YYYY")
        expect(res).toEqual("2023")
    })
    test('YYYY-MM 格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format("YYYY-MM")
        expect(res).toEqual("2023-08")
    })
    test('YYYY-MM-DD 格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format("YYYY-MM-DD")
        expect(res).toEqual("2023-08-18")
    })
    test('YYYY-MM-DD HH 格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format('YYYY-MM-DD HH')
        expect(res).toEqual("2023-08-18 11")
    })
    test('HH:MM:SS格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format('HH:MM:SS')
        expect(res).toEqual("11:46:04")
    })
    test('YYYY-MM-DD HH:MM格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format('YYYY-MM-DD HH:MM')
        expect(res).toEqual('2023-08-18 11:46')
    })
    test('YYYY-MM-DD格式', () => {
        const res = dateKit.setDate('2023-08-18 11:46:04').format('YYYY-MM-DD')
        expect(res).toEqual('2023-08-18')
    })

    // test('测试几年前', () => {
    //     const res = dateKit.getPastTime('2020')
    //     expect(res).toEqual({"text": "year ago", unit: 'year', "time": 3 })
    // })

    test('测试几天前', () => {
        const res = dateKit.getPastTime('2023-09-18 11:46:04')
        expect(res).toEqual(res)
    })

    // test('测试几分钟前', () => {
    //     const res = dateKit.getPastTime('2023-10-07 16:16:04')
    //     expect(res).toEqual({"text": "minutes ago", "time": 1})
    // })

    // test('测试几秒钟前', () => {
    //     const res = dateKit.getPastTime('2023-10-07 16:20:04')
    //     expect(res).toEqual({"text": "minutes ago", "time": 5})
    // })
})
