import { validatePhone } from "../src/module/phone"

describe('validate phone', () => {
    test('测试空手机号', () => {
        const res = validatePhone('')
        expect(res).toBe(false)
    })

    test('联通手机号', () => {
        const res = validatePhone('18913345674')
        expect(res).toBe(true)
    })

    test('电信手机号', () => {
        const res = validatePhone('13322345678')
        expect(res).toBe(true)
    })

    test('移动手机号', () => {
        const res = validatePhone('18786014642')
        expect(res).toBe(true)
    })

    test('错误手机号', () => {
        const res = validatePhone('98765432142')
        expect(res).toBe(false)
    })

    test('纯数字手机号', () => {
        const res = validatePhone(98765432142)
        expect(res).toBe(false)
    })
})