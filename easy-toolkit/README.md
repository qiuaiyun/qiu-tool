

# easy-toolkit🍓

#### 介绍

前端项目开发常用web工具类，包括**手机号码**、**身份证验证**、**中文校验**、**获取日期和根据日期格式获取日期或者转换日期格式**、**对象数组根据key分组**、**邮箱格式校验**、**获取历史时间**，**时间差**，**数组去重**，**多个对象数组去重**等工具方法，减少多余包引用，增加工具类复用性，提高开发效率。



#### 软件架构

工具包基于`TypeScript 5.2.2`进行开发，tsc编译，经过jest工具测试。支持vue2、vue3、原生HTML和React项目。


### 1、安装教程

##### 1、npm安装

```javascript
$ npm i easy-toolkit -D
```

### 2、工具介绍

1. `validateEmail`：邮箱校验，返回布尔值，true表示校验通过，false表示格式错误；
2. `dateKit`：日期工具，返回日期字符串；
3. `deepClone`：升级后的深度拷贝工具；
4. `DateFormat`：时间格式类型；
5. `weekDays`：周一到周天字符串数组；
6. `groupByKey`：对象数组分组；
7. `duplicationArr`: 字符串数组去重和对象数组去重；
8. `validatePhone`: 校验手机号格式是否正确;
9. `validatePassword`: 校验密码强度是否合格，分为强(powerful)，中(middle)和弱(weak)模式；
10. `getUUID`: 获取唯一uuid值，长度为32位字符串；
11. `Shuffle`: 随机获取中文或者英文名字，支持单个或者多个，单个名字以字符串形式返回，多个名字以数组形式返回。
12. [2024-9-13 新增] `zhCnTime`: 获取中文时间字符串，eg:2024年09月13日 14点02分25秒


### 3、配置使用

在需要的文件内导入模块。

#### 3.1 validateEmail

举例：validateEmail 用法，目前支持qq邮箱格式校验。

```js
import { validateEmail } from "easy-toolkit";

// 传入邮箱值
 const res = validateEmail('6666@qq.com')
 console.log(res) // true

 const res = validateEmail('6666@1qq.com')
 console.log(res) // false
```

#### 3.2 dateKit

时间工具，可以获取当前时间，或者根据支持格式**获取当前时间**，也能**转化时间格式**，**计算两个时间差值**和**获取指定日期是星期几**。

当前支持日期格式：

```ts
type DateFormat =
"YYYY" | 
"YYYY-MM" | 
"YYYY-MM-DD" | 
"YYYY/MM/DD" |
"YYYY-MM-DD HH" | 
"YYYY/MM/DD HH" |
"YYYY-MM-DD HH:MM" |
"YYYY/MM/DD HH:MM" |
"YYYY-MM-DD HH:MM:SS" |
"YYYY/MM/DD HH:MM:SS" | 
"HH:MM:SS";
```

##### 获取当前时间

不传入时间格式，获取的是"YYYY-MM-DD HH:MM:SS"格式的时间。

```ts
import { dateKit } from "easy-toolkit";

const res = dateKit.format()
console.log(res) // 2023-08-18 11:46:04
```

##### 指定获取时间格式

```ts
import { dateKit } from "easy-toolkit";

const res = dateKit.format("YYYY-MM-DD HH:MM")
console.log(res) // 2023-08-18 11:46
```

##### 时间格式转化

```ts
import { dateKit } from "easy-toolkit";

const res = dateKit.setDate('2023-08-18 11:46:04').format('YYYY-MM-DD HH')
console.log(res) // 2023-08-18 11

const res = dateKit.setDate('2023-08-18 11:46:04').format('YYYY-MM-DD HH:MM')
console.log(res) // 2023-08-18 11:46
```

##### 获取时间格式类型

```js
import type { DateFormat } from "easy-toolkit";

DateFormat = 
    "YYYY" | 
    "YYYY-MM" | 
    "YYYY-MM-DD" | 
    "YYYY/MM/DD" | 
    "YYYY-MM-DD HH" | 
    "YYYY/MM/DD HH" | 
    "YYYY-MM-DD HH:MM" | 
    "YYYY/MM/DD HH:MM" | 
    "YYYY-MM-DD HH:MM:SS" | 
    "YYYY/MM/DD HH:MM:SS" | 
    "HH:MM:SS";
```

##### 当前中文时间字符串
`api：dateKit.zhCnTime(format)`,
当前支持格式：YYYY|YYYY-MM|YYYY-MM-DD HH|YYYY-MM-DD HH:MM|HH:MM:SS|YYYY-MM-DD HH:MM:SS,
默认格式：YYYY-MM-DD HH:MM:SS
```js
import { dateKit } from "easy-toolkit";
// 默认可以不传递format格式，它默认使用：YYYY-MM-DD HH:MM:SS
const res = dateKit.zhCnTime()

console.log(res)
// Expected output: 2024年09月13日 14点02分25秒
```
此外，zhCnTime()允许传递自定义时间格式和时间字符串或者时间对象Date，来显示中文时间；
```js
const res = dateKit.zhCnTime('HH:MM:SS', '2023-04-05 15:45:04')
console.log(res);
// Expected output: 15点45分04秒

const res = dateKit.zhCnTime('HH:MM:SS', new Date())
console.log(res);
// Expected output: 14点59分26秒

// 注意：传入时间字符串或者时间对象，必须符合规范，否则视为无效时间返回
// 无效案例1
const res = dateKit.zhCnTime('HH:MM:SS', 'sdfsbdfsdf')
console.log(res)
// Expected output: sdfsbdfsdf Is Invalid Date.

// 无效案例2
const res = dateKit.zhCnTime('HH:MM:SS', new Date('sdfsbdfsdf'))
console.log(res)
// Expected output:  Invalid Date Is Invalid Date.
```

##### 获取星期字符

```ts
import { dateKit } from "easy-toolkit";

// 指定时间星期几
const res = dateKit.getWeekOfDate("2023-08-18 11:46:04")
console.log(res) // 星期五

// 获取当天时间星期几
const res = dateKit.getWeekOfDate()
console.log(res) // 星期六
```

##### 获取当前时间戳

```ts
import { dateKit } from "easy-toolkit";

const res = dateKit.getTimeStamp()
console.log(res) // 1695385969397
```

##### 计算时间段差值

获取时间段差值，建议使用完整的YYYY-MM-DD HH:MM:SS时间格式，计算更精确。

```typescript
import { dateKit } from "easy-toolkit";

const res = dateKit.calcDateDifference('2023-08-18 11:46:04', '2023-08-19 19:30:04')
console.log(res) // { day: 1, hours: 7.73, timeStr: '1天7.73时44分钟', minutes: 44 }


const res = dateKit.calcDateDifference('2023-08-18', '2023-09-19')
console.log(res) // { day: 32, hours: 0, timeStr: '32天0时0分钟', minutes: 0 }
```

##### 获取当前月总天数

返回当前月总天数数组

```js
import { dateKit } from "easy-toolkit";
// 当前打印10月份
const res = dateKit.getTotalDays()
console.log(res);
// [
       1,  2,  3,  4,  5,  6,  7,  8,  9,
      10, 11, 12, 13, 14, 15, 16, 17, 18,
      19, 20, 21, 22, 23, 24, 25, 26, 27,
      28, 29, 30, 31
    ]
```

##### 历史时间

dateKit.getPastTime('2023-09-18 11:46:04')，返回几年前，几天前，几小时前，几分钟前，几秒钟前。

**注意：参数必传，否则返回值有误**

返回对象：{"text": "year ago", unit: 'year', "time": 3 }

```js
import { dateKit } from "easy-toolkit";
/**
   * Get historical time
   * format: YYYY-MM-DD HH:MM:SS
   *         | YYYY
   *         | YYYY-MM
   *         | YYYY-MM-DD HH
   *         | YYYY-MM-DD HH:MM
   * @param { timestamps: string } date of value
   * @returns object{ time: xxx, unit: 'day', text: 'minutes ago'}
   */
const res = dateKit.getPastTime('2023-09-18 11:46:04')
console.log(res);
// {"text": "day ago", unit: 'day', "time": 19 }
```



#### 3.3 deepClone

深度拷贝数据，包括对象和数组。

```typescript
import { deepClone } from "easy-toolkit";

const arr = [{ name: '张三', age: 25 }, true, [1,2,3,4]]
const res = deepClone(arr)
console.log(res) // [{ name: '张三', age: 25 }, true, [1,2,3,4]]
```

#### 3.4 groupByKey

针对对象数组分组，回显分组后的数组，可以根据配置属性进行分组。

```typescript
import { groupByKey } from "easy-toolkit";

// 分组数据
const storehouse = [
    { name: "asparagus", type: "vegetables", quantity: 5 },
    { name: "bananas", type: "fruit", quantity: 0 },
    { name: "goat", type: "meat", quantity: 23 },
    { name: "cherries", type: "fruit", quantity: 5 },
    { name: "fish", type: "meat", quantity: 22 },
];
const list = groupByKey(storehouse, ({ type }) => type))
console.log(list);
// 输出结果
{
    "vegetables": [
        {
            "name": "asparagus",
            "type": "vegetables",
            "quantity": 5
        }
    ],
    "fruit": [
        {
            "name": "bananas",
            "type": "fruit",
            "quantity": 0
        },
        {
            "name": "cherries",
            "type": "fruit",
            "quantity": 5
        }
    ],
    "meat": [
        {
            "name": "goat",
            "type": "meat",
            "quantity": 23
        },
        {
            "name": "fish",
            "type": "meat",
            "quantity": 22
        }
    ]
}
```

#### 3.5 duplicationArr

字符串数组和对象数组去重

```typescript
import { duplicationArr } from "easy-toolkit";

// jest测试结果
describe('duplication', () => {
    const storehouse = [
        { name: "asparagus", type: "vegetables", quantity: 5 },
        { name: "bananas", type: "fruit", quantity: 0 },
        { name: "goat", type: "meat", quantity: 23 },
        { name: "cherries", type: "fruit", quantity: 5 },
        { name: "fish", type: "meat", quantity: 22 },
    ];
    const result = [
        { name: 'asparagus', type: 'vegetables', quantity: 5 },
        { name: 'bananas', type: 'fruit', quantity: 0 },       
        { name: 'goat', type: 'meat', quantity: 23 }
    ]
    const arr = [1,2,5,2,5,2,541,25,555]
    test('字符串数组去重', () => {
        const res = duplicationArr(arr)
        expect(res).toEqual([ 1, 2, 5, 541, 25, 555 ])
    })
    test('对象数组去重', () => {
        const res = duplicationArr(storehouse, 'type')
        expect(res).toEqual(result)
    })
})
```

#### 3.6 validatePhone

手机号格式正确返回true，否则返回false

```typescript
import { validatePhone } from "easy-toolkit";

const res = validatePhone('18786014642')
console.log(res) // true 

const res = validatePhone('18786014')
console.log(res) // false 
```

#### 3.7 validatePassword

密码强度校验分为三个等级，分为强(powerful)，中(middle)和弱(weak) 模式，默认是强(powerful)模式，密码字符串至少8个字符。

```ts
import { validatePassword } from "easy-toolkit";

// 测试弱密码
const res = validatePassword('abc123789', 'weak')
console.log(res) // true

// 中等密码 含有字母大小写和数字
const res = validatePassword('Abc1234567', 'middle')
console.log(res) // true

// 强密码，含有字母大小写、数字和特殊字符!@#$%^&*.-_
const res = validatePassword('Abc123!@13.0', 'powerful')
console.log(res) // true
```

#### 3.8 getUUID

获取唯一uuid值，长度为32位字符串，当前支持分隔符：'-' | '_' | '*' | '#' | '&'

```js
import { getUUID } from "easy-toolkit";

// 不传递分割字符
const uuid = getUUID()
console.log(uuid)
// 35bff70be55045cda9caf6aa7efe7f63


// 传递分割字符
const uuid = getUUID('-')
console.log(uuid)
// 4483a454-8c60-47eb-a42e-486a392da00a
```

#### 3.9 Shuffle

通过这个类可以获取单个或者多个随机名字，甚至更多，包括中文和英文。

```js
import { Shuffle } from "easy-toolkit";

// 测试获取一个中文名字
const res = Shuffle.chineseNames()
console.log(res);
// 成七

// 测试获取10个中文名字，甚至更多
const res = Shuffle.chineseNames(10)
console.log(res);
// [
      '蓝娜',   '施陆',
      '慕容丽', '梁三',
      '舒一',   '白丽',
      '申屠一', '赫连明',
      '宋明',   '明强'
    ]

// 测试获取一个英文名字
const res = Shuffle.englishNames()
console.log(res);
// Ulaveq

// 测试获取10个英文名字，甚至更多
const res = Shuffle.englishNames(10)
console.log(res);
// [
      'Aworod',   'Eluqejet',
      'Ujofujom', 'Iqileg',
      'Eyode',    'Aqiwu',
      'Idixije',  'Igeya',
      'Oxalizin', 'Afara'
    ]
```

