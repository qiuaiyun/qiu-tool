const config = {
    preset: 'ts-jest',
    verbose: true,
    collectCoverage: true,
    coverageReporters: ['html', 'text'],
    coverageDirectory: 'coverage',
    testMatch:[
        '**/__tests__/**/*.test.(ts|js|jsx|tsx)',
        '**/?(*.)+(spec|test).(ts|js|jsx|tsx)'
    ],
    testEnvironment: 'node',
    moduleFileExtensions: ['js', 'ts', 'tsx', 'jsx'],
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.json'
        }
    }
}

module.exports = config;