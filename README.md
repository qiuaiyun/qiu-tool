

# qnyutils

#### 介绍

前端项目开发常用js工具类，包括手机号码、身份证验证、中文校验、历史时间校验等工具方法，减少多余包引用，增加工具类复用性，提高开发效率



#### 软件架构

本工具库是基于纯JavaScript开发的，后续会扩展支持typescript。


#### 安装教程

##### 1、npm安装

```javascript
$ npm i qnyutils
```

##### 2、配置使用

**全部注入**：在vue2项目中入口文件导入依赖并注册全局

```js
import Vue from "vue";
import Qnyutils from "qnyutils";

Vue.prototype.$Qnyutils = window.Qnyutils = Qnyutils;
```

**按需引入**：比如需要时间工具和手机校验工具

```js
// 按功能导入
import { getFormatTime, validateFormatPhone } from "qnyutils";

// 在js中使用
console.log(getFormatTime(), validateFormatPhone('123456'))
```

js/template 中基本使用案例：

```js
// js 输入中文检验
this.$Qnyutils.chineseCheck('222') // false
// template 输入中文检验
$Qnyutils.chineseCheck('222') // false
```

更多工具类访问：[## 目前集成的方法使用文档](#content)



#### 使用说明

1.  工具目前仅支持vue2.0或者vue3.0+js模式的npm安装依赖的项目
2.  Gitee 官方使用手册 [https://gitee.com/qiuaiyun/qiu-tool](https://gitee.com/qiuaiyun/qiu-tool)

#### 参与贡献

本套工具库解释权归布衣前端所有

1、贡献者：布衣前端



参与贡献：

QQ：1766226354



<h2 id="content">目前集成的方法使用文档</h2>



[1、手机号码格式校验](#phone)

[2、中文校验](#china)

[3、日期格式转化](#date)

[4、邮箱格式校验](#email)

[5、数字+字母[6-20]格式校验](#password)

[6、数字类型校验](#number)

[7、保留两位小数格式输出](#twoDecimal)

[8、对象浅拷贝](#copyClone)

[9、历史时间格式，几秒钟前，几分钟前，几天前](#historytime)

[10、校验身份证号是否合法](#ident)


<h5 id="phone">手机号校验</h5>

校验手机号格式是否正确，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.validateFormatPhone(options)

// 示例
this.$Qnyutils.validateFormatPhone('12345678942') // 返回 false
```

<h5 id="china">中文校验</h5>

校验输入字符串是否是纯中文，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.chineseCheck(options)

// 示例
this.$Qnyutils.chineseCheck('12345678942') // 返回 false
```



<h5 id="date">日期格式转化</h5>

日期格式目前支持自定义年月日之间的分隔符：`'/', '-', '.', '·', '_', ''` ，默认 `-` ，日期格式支持：`YYYY-MM-DD HH:MM:SS`， `YYYY-MM-DD HH:MM`，`YYYY-MM-DD`，`HH:MM:SS`，`YYYY-MM` ，默认是`YYYY-MM-DD HH:MM:SS`

```js
// 格式
* @param {*} time 可选，默认返回当前时间， 而当前时间，js中一般由new Date()获得，格式示例：Sun Nov 28 2021 19:46:23 GMT+0800 (中国标准时间)
* @param {*} format 可选，时间格式，默认：YYYY-MM-DD HH:MM:SS
* @param {*} separator 可选，分隔符， 默认：'-'
this.$Qnyutils.getFormatTime(time, format, separator)

// 示例
this.$Qnyutils.getFormatTime(new Date(), 'YYYY-MM-DD') // 返回， 2021-09-12
this.$Qnyutils.getFormatTime() // 返回， 2021-09-12 15:21:32          
```



<h5 id="email">邮箱格式校验</h5>

邮箱格式校验，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.validateEmail(options)

// 示例
this.$Qnyutils.validateEmail('11222') // 返回false
this.$Qnyutils.validateEmail('11222@qq.com') // 返回true
```



<h5 id="password">数字+字母[6-20]格式校验</h5>

专门针对密码格式校验的方法，必须是数字+字母，且长度最小为6，最大为20

```js
// 格式
options: [string]
this.$Qnyutils.validatePassWord(options)

// 示例
this.$Qnyutils.validatePassWord('11222@qq.com') // 返回 false
this.$Qnyutils.validatePassWord('11222qqcom') // 返回 true
```

<h5 id="number">数字类型校验</h5>

数字类型，专门针对纯数字校验，其他符合会被替换为空，返回纯数字，没有限制长度，如果输入没有中文，增返回空，否则返回字符串

```js
// 格式
options: [string]
this.$Qnyutils.validateNumber(options)

// 示例
this.$Qnyutils.validateNumber('11222qqcom') // 返回 11222
```



<h5 id="twoDecimal">保留两位小数格式输出</h5>

保留两位小数，输入的必须是数字类型，且能为负数，返回保留两位小数的字符串

```js
// 格式
options: [string]
this.$Qnyutils.getTwoDecimal(options)

// 示例
this.$Qnyutils.getTwoDecimal('11222qqcom') // 返回 Nan 错误示例
this.$Qnyutils.getTwoDecimal('11222.1255') // 返回 11222.13 正确示例
this.$Qnyutils.getTwoDecimal('-11222.1255') // 返回 -11222.13 正确示例
```



<h5 id="copyClone">对象深度拷贝</h5>

对象深度拷贝，可以用在对象属性或者其他类型数据的备份上。

```js
// 格式
options:[object|array]
this.$Qnyutils.deepCloneObj(options)

// 示例
let obj = {age:12, name: '张三'}
this.$Qnyutils.deepCloneObj(obj) // 返回 newobj
```

<h5 id="historytime">历史时间格式，几秒钟前，几分钟前，几天前</h5>

历史时间格式化，例如，几秒前，几天前，几分钟前*，*时间字符串： 例如：2021-11-29 16:05:10

```js
// 格式
options:[string]: '完整时间字符串，包括年月日时分秒'
this.$Qnyutils.getHistoryTime(options)

// 正确示例
this.$Qnyutils.getHistoryTime('2021-11-29 16:05:10') // 返回 几天前
```

<h5 id="ident">校验身份证是否合法</h5>

身份证合法，返回true，错误返回false

```js
// 格式
options:[string]: '身份证号'
this.$Qnyutils.validateIdent(options)

// 正确示例
this.$Qnyutils.validateIdent('522328199609021714') // 返回 true 
```
