

# qloadings

#### 介绍

web页面加载动画，支持circle(圆图标)，spinner(变化图标)，notch(轮齿图标)，refresh(刷新图标)，setting(重置图标)，支持用户自定义加载图案和加载文本，前端项目开发常用的页面加载动画Loading，支持用户自定义加载动画类型和加载文本，背景颜色设置，模块化区域加载等等灵活功能。

#### 软件架构

本工具库是基于纯JavaScript，CSS3进行开发的，后续会扩展支持typescript。


#### 安装和使用

##### 1、npm安装

```javascript
$ npm install qloadings
```

注意：如果npm官方镜像下载不了推荐使用加速镜像，比如淘宝镜像。

```
全局切换镜像源：npm config set registry http://registry.npm.taobao.org
查看镜像源使用状态：npm get registry
全局切换官方镜像源：npm config set registry https://registry.npmjs.org
```



##### 2、配置使用和部分属性自定义

**全部注入**：在vue2项目中入口文件导入依赖并注册全局，注意：加载动画默认是持续3000ms，必须自己调用取消方法才会停止。

```js
import Vue from "vue";
import QLoading from "qloadings";
// 加载样式
import "qloadings/qloading.css";

// 自定义配置
QLoadings.configure({
    title: '数据加载中，请稍后...', // 自定义加载名称
    fontColor: "#FCF7FF", // 自定义字体颜色
    defineIcon: 'notch', // 选择动画图标，notch：缺口实心圆，circle：圆点圆，refresh：刷新圆，setting：轮齿圆， spinner：变化圆
    parent: '.content-box', // 自定义父级盒子，没有可以为空
    zIndex: '9999', // 自定义加载动画层级 
    bgColor: "#4E8397", // 自定义背景颜色
    bgOpacity: '0.8', // 自定义加载动画背景透明度
    duration: 3000 // 自定义显示时间，默认持续时间3000ms
})
// 直接启动
 QLoadings.execute();
// 直接关闭
 QLoadings.destroy();

// 挂在Vue实例，然后启动/关闭
Vue.prototype.$QLoading = window.QLoading = QLoading;
```

**注意： parent: '.content-box', // 自定义父级盒子，如果你要自定义加载动画到某个盒子，必须给盒子高度或者宽度，具体宽高根据实际页面来设置即可，这样才能正常显示动画，否则显示故障** 

##### 3、自定义父级容器

自定义父级旋转容器必须设置父级容器的相对定位样式 position: relative; 且有固定高度，否则不显示或者显示错乱。

举个例子：

```html
<div class="content-box">
    自定义父级容器注意事项---qloadings 布衣前端
</div>
```

```css
.content-box{
    height: 300px;
    border: 1px solid #eee;
    position: relative;
}
```

