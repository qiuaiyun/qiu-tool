

# qnyutils🎃

#### 介绍

前端项目开发常用js工具类，包括**手机号码**、**身份证验证**、**中文校验**、**获取日期和根据日期格式获取日期或者转换日期格式**、**对象数组根据key分组**、**邮箱格式校验**、**获取历史时间**，**时间差**，**数组去重**，**多个对象数组去重**等工具方法，减少多余包引用，增加工具类复用性，提高开发效率。



#### 软件架构

本工具库是基于纯JavaScript开发的，后续会扩展支持typescript。


#### 安装教程

##### 1、npm安装

```javascript
$ npm i qnyutils -D
```

##### 2、配置使用

**全部注入**：在vue2项目中入口文件导入依赖并注册全局

```js
import Vue from "vue";
import Qnyutils from "qnyutils";

Vue.prototype.$Qnyutils = window.Qnyutils = Qnyutils;
```

**按需引入**：比如需要时间工具和手机校验工具

```js
// 按功能导入
import { getFormatTime, validateFormatPhone } from "qnyutils";

// 在js中使用
console.log(getFormatTime(), validateFormatPhone('123456'))
```

js/template 中基本使用案例：

```js
// js 输入中文检验
this.$Qnyutils.chineseCheck('222') // false
// template 输入中文检验
$Qnyutils.chineseCheck('222') // false
```

更多工具类访问：[## 目前集成的方法使用文档](#content)



#### 使用说明

1.  工具目前仅支持vue2.0或者vue3.0+js模式的npm安装依赖的项目
2.  Gitee 官方使用手册 [https://gitee.com/qiuaiyun/qiu-tool](https://gitee.com/qiuaiyun/qiu-tool)

#### 参与贡献

本套工具库解释权归布依前端所有，盗版必究

1、贡献者：布依前端



参与贡献：

QQ：1766226354



### 布依前端工具库说明

[TOC]



#### 手机号校验

校验手机号格式是否正确，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.validateFormatPhone(options)

// 示例
this.$Qnyutils.validateFormatPhone('12345678942') // 返回 false
```

#### 中文校验

校验输入字符串是否是纯中文，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.chineseCheck(options)

// 示例
this.$Qnyutils.chineseCheck('12345678942') // 返回 false
```

#### 获取日期和日期格式转化

支持获取当前日期和传入日期格式化

支持格式：`YYYY、YYYY-MM、YYYY-MM-DD、YYYY-MM-DD HH、YYYY-MM-DD HH:MM、YYYY-MM-DD HH:MM:SS、HH:MM:SS`

用法：

```js
/**
 * @description 将标准时间转成需要格式返回，默认年月日时分秒格式，
 *              如果不传入时间或者格式，默认获取当前时间 YYYY-MM-DD HH:MM:SS 返回
 * @param {*} format 时间格式，默认：YYYY-MM-DD HH:MM:SS
 * @param {*} time 当前时间，js中一般由new Date()获得，格式示例：Sun Nov 28 2021 19:46:23 GMT+0800 (中国标准时间)
 * @returns string 年-月-日 xx:xx:xx 默认
 */
this.$Qnyutils.getFormatTime(time, format)

// 示例
// 获取当前时间含时分秒
this.$Qnyutils.getFormatTime() // 返回， 2023-08-18 11:46:04

// 获取当前日期，年月日，format= 'YYYY-MM-DD'，time可以写成'', null
this.$Qnyutils.getFormatTime(null, 'YYYY-MM-DD') // 返回， 2023-08-18 

// 获取当前日期，format= 'YYYY-MM-DD HH'，time可以写成'', null
this.$Qnyutils.getFormatTime(null, 'YYYY-MM-DD HH') // 返回， 2021-09-12 15  

// 获取当前年份 format= 'YYYY'
this.$Qnyutils.getFormatTime(null, 'YYYY') // 返回 2023

// 日期time='2023-08-12 11:10:10'转化成format= 'YYYY-MM-DD'
this.$Qnyutils.getFormatTime('2023-08-12 11:10:10', 'YYYY-MM-DD') // 返回 2023-08-12

// 日期time='2023-08-12 11:10:10'转化成format= 'YYYY-MM-DD HH:MM'
this.$Qnyutils.getFormatTime('2023-08-12 11:10:10', 'YYYY-MM-DD HH:MM') // 返回 2023-08-12 11:10

// 日期time='2023-08-12 11:10:10'转化成format= 'HH:MM:SS'
this.$Qnyutils.getFormatTime('2023-08-12 11:10:10', 'HH:MM:SS') // 返回 11:10:10
```



#### 邮箱格式校验

邮箱格式校验，正确返回true，错误返回false

```js
// 格式
options: [string]
this.$Qnyutils.validateEmail(options)

// 示例
this.$Qnyutils.validateEmail('11222') // 返回false
this.$Qnyutils.validateEmail('11222@qq.com') // 返回true
```



#### 数字+字母[6-20]格式校验

专门针对密码格式校验的方法，必须是数字+字母，且长度最小为6，最大为20

```js
// 格式
options: [string]
this.$Qnyutils.validatePassWord(options)

// 示例
this.$Qnyutils.validatePassWord('11222@qq.com') // 返回 false
this.$Qnyutils.validatePassWord('11222qqcom') // 返回 true
```

#### 数字类型校验

数字类型，专门针对纯数字校验，其他符合会被替换为空，返回纯数字，没有限制长度，如果输入没有中文，增返回空，否则返回字符串

```js
// 格式
options: [string]
this.$Qnyutils.validateNumber(options)

// 示例
this.$Qnyutils.validateNumber('11222qqcom') // 返回 11222
```



#### 保留两位小数格式输出

保留两位小数，输入的必须是数字类型，且能为负数，返回保留两位小数的字符串

```js
// 格式
options: [string]
this.$Qnyutils.getTwoDecimal(options)

// 示例
this.$Qnyutils.getTwoDecimal('11222qqcom') // 返回 Nan 错误示例
this.$Qnyutils.getTwoDecimal('11222.1255') // 返回 11222.13 正确示例
this.$Qnyutils.getTwoDecimal('-11222.1255') // 返回 -11222.13 正确示例
```

#### 对象深度拷贝

对象深度拷贝，可以用在对象属性或者其他类型数据的备份上。

```js
// 格式
options:[object|array]
this.$Qnyutils.deepCloneObj(options)

// 示例
let obj = {age:12, name: '张三'}
this.$Qnyutils.deepCloneObj(obj) // 返回 newobj
```

#### 历史时间格式

历史时间格式化，主要是当前时间和历史时间对比，例如，几秒前，几天前，几分钟前*，*时间字符串例如：2021-11-29 16:05:10

```js
// 格式
options:[string]: '完整时间字符串，包括年月日时分秒'
this.$Qnyutils.getHistoryTime(options)

// 正确示例
this.$Qnyutils.getHistoryTime('2021-11-29 16:05:10') // 返回 几天前
```

#### 校验身份证是否合法

身份证合法，返回true，错误返回false

```js
// 格式
options:[string]: '身份证号'
this.$Qnyutils.validateIdent(options)

// 正确示例
this.$Qnyutils.validateIdent('522328199609021714') // 返回 true 
```

#### 数组去重[单个数组，对象数组]

```js
/**
 * 多个数组去重, 可是对象数组和字符串数组
 * @param {*} key 对象数组key，单个数组不能省略，设置为null即可
 * @param  {...any} _arguments 接收多个数组
 * @returns array
 */
this.$Qnyutils.multDeduplication = (key, ..._arguments)
```



##### 单个数组去重

```js
const numArr = [1, 2, 5, 6, 8, 7, 6, 5, 6]
const strArr = ['a', 'a', 'b', 'c', 'd', 'f', 'd']
const arr1 = this.$Qnyutils.multDeduplication(null, numArr)
const arr2= this.$Qnyutils.multDeduplication(null, strArr)
// arr1输出结果
[1, 2, 5, 6, 8, 7]
// arr2输出结果
['a', 'b', 'c', 'd', 'f']
```

##### 对象数组去重

单个/多个对象数组去重，需要依赖对象key进行划分，key必传参数，如果传递key为null，则无法去重，如果key值错误，程序出现警告。

```js
const list = [
    { price: 95.00, name: '三国演义'},
    { price: 100.00, name: '西游记'},
    { price: 68.00, name: '红楼梦'},
    { price: 88.00, name: '水浒传'},
    { price: 88.00, name: '水浒传'}
]
const list2 = [
    { price: 915.00, name: '三国演义'},
    { price: 10.00, name: '西游记'},
    { price: 6.00, name: '红楼梦'},
    { price: 8.00, name: '水浒传'},
    { price: 88.00, name: '水浒传'}
]
const arr = this.$Qnyutils.multDeduplication('price', list, list2)
// arr输出结果
[
    {
        "price": 95,
        "name": "三国演义"
    },
    {
        "price": 100,
        "name": "西游记"
    },
    {
        "price": 68,
        "name": "红楼梦"
    },
    {
        "price": 88,
        "name": "水浒传"
    },
    {
        "price": 915,
        "name": "三国演义"
    },
    {
        "price": 10,
        "name": "西游记"
    },
    {
        "price": 6,
        "name": "红楼梦"
    },
    {
        "price": 8,
        "name": "水浒传"
    }
]
```

#### 计算时间区间差值

计算时间区间差值，返回相差天数、小时和分钟

用法

```js
/**
 * 计算时间区间差值，返回相差天数、小时和分钟
 * @param {*} startTime 开始时间 2023-08-12 11:10:10
 * @param {*} endTime 结束时间 2023-08-14 20:50:10
 * @returns 
 * {
 *    day: days,
 *    hours: hour,
 *    timeStr: time,
 *    minutes
 *  }
 */
this.$Qnyutils.calcDateDifference (startTime, endTime)

// 调用方法
const value = this.$Qnyutils.calcDateDifference ('2023-08-12 11:10:10', '2023-08-14 20:50:10')
console.log(value)
// 打印结果
{
    "day": 2,
    "hours": 9.67,
    "timeStr": "2天9.67时40分钟",
    "minutes": 40
}

```

#### 对象数组分组

针对对象数组分组，回显分组后的数组，可以根据配置属性进行分组。



```js
// 分组数据
const storehouse = [
    { name: "asparagus", type: "vegetables", quantity: 5 },
    { name: "bananas", type: "fruit", quantity: 0 },
    { name: "goat", type: "meat", quantity: 23 },
    { name: "cherries", type: "fruit", quantity: 5 },
    { name: "fish", type: "meat", quantity: 22 },
];
const list = this.$Qnyutils.groupByKey(storehouse, ({ type }) => type))
console.log(list);
// 输出结果
{
    "vegetables": [
        {
            "name": "asparagus",
            "type": "vegetables",
            "quantity": 5
        }
    ],
    "fruit": [
        {
            "name": "bananas",
            "type": "fruit",
            "quantity": 0
        },
        {
            "name": "cherries",
            "type": "fruit",
            "quantity": 5
        }
    ],
    "meat": [
        {
            "name": "goat",
            "type": "meat",
            "quantity": 23
        },
        {
            "name": "fish",
            "type": "meat",
            "quantity": 22
        }
    ]
}
```

