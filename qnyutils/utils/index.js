/**
 * 日志打印
 * @param {*} msg 日志文本
 * @param {*} type 日志类型 log，info，warn
 */
export const customLog = (msg, type='warn') => {
    console[type](msg);
}


// 判断日期格式是否正确
export const isCheckDateAndTime = (dateTime) => {
    const reg = /^(\d{4})(-|\/)(\d{2})\2(\d{2}) (\d{2}):(\d{2}):(\d{2})$/;    
    const res = dateTime.match(reg);
    if (res === null) return false; 
    const d = new Date(res[1], res[3]-1, res[4], res[5], res[6], res[7]);    
    return (d.getFullYear()==res[1]&&(d.getMonth()+1)==res[3]&&d.getDate()==res[4]&&d.getHours()==res[5]&&d.getMinutes()==res[6]&&d.getSeconds()==res[7]);
}