const express = require('express');
const path = require('path');
const app = express();

/**默认返回首页 */
app.use(express.static(path.join(__dirname, 'public')));

app.listen(8089, ()=> {
    console.log('network at: http://127.0.0.1:8089')
})