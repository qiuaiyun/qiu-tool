import { customLog } from "../utils/index.js";

/**
 * 多个数组去重, 可是对象数组和字符串数组
 * @param {*} key 对象数组key，单个数组不能省略，设置为null即可
 * @param  {...any} _arguments 接收多个数组
 * @returns array
 */
const multDeduplication = (key, ..._arguments) => {
    if (_arguments.length === 0) return
    if (Array.isArray(_arguments)) {
        customLog(`传入参数【${_arguments}】不是数组类型，无法进行去重`)
        return
    }
    
    const newArr = _arguments.flat();
    let arraySingle = [];

    if (key) {
        arraySingle = newArr.reduce((pre, cur) => {
            // 判断key是否存在对象中，否则抛出异常
            const _objKeys = Object.keys(cur);
            if (!_objKeys.some(k => k === key)) {
                customLog(`参数${key}无法在当前${JSON.stringify(cur)}对象属性中进行匹配，解析异常，请核对后在试`)
                return false;
            }
            // 寻找相同值的key
            const currentIndex = pre.findIndex(el => el[key] === cur[key])
            // 如果不存在，则添加
            if (currentIndex === -1) {
                pre.push(cur)
            }
            return pre;
        }, []);
    }
    else arraySingle = newArr.reduce((pre, cur) => pre.some(e => e === cur) ? pre: [...pre, cur], [])

    return arraySingle;
}

export default multDeduplication;