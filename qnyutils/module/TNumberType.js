/**
 * 过滤数字并返回
 * @param {*} val 校验字符串
 * @returns 
 */
const validateNumber = val => {
    if(val != null){
        if(val.length == 1){
            val = val.replace(/[^1-9]/g,'')
        }
        else{
            val = val+'';
            val = val.replace(/\D/g,'')
        }
        return val;
    }else {
        return val;
    }
};

export default validateNumber;