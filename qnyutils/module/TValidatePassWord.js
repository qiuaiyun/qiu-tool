import { customLog } from "../utils/index.js";

/**
 * 验证密码 6-20位 数字+字母
 * @param {*} value 密码
 * @returns 
 */
 const validatePassWord = value => {
    if (!value) {
        customLog('参数password不能为空')
        return
    }

    let reg = /^(?!\D+$)(?![^a-zA-Z]+$)\S{6,20}$/;
    return !reg.test(value)? false:true;
}

export default validatePassWord;