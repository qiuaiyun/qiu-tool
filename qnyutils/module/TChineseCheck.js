import { customLog } from "../utils/index.js";
/**
 * 全中文名称校验，主要用于输入用户名之类校验
 * @param {*} value 检验值
 * @param {*} len 长度，默认为 4
 * @returns 
 */
const chineseCheck = (value) => {
    const regs = /[@#\$%\^&\*]+/g;
    if (!value || regs.test(value)) {
        customLog(`传入变量【${value}】不能为空、null、undefined以及其他特殊字符`)
        return
    }
    const reg = /^[\u4e00-\u9fa5]{0,}$/;
    if (value) {
        return reg.test(value)
    } else return false
}
export {
    chineseCheck
}
export default chineseCheck; 