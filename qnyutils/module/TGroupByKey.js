import { customLog } from "../utils/index.js";

/**
 * reduce实现 
 * 根据类型实现数组数据分组方法
 * 以对象形式返回结果
 * @param {*} array 对象数组
 * @param {*} callBack 回调函数
 * @returns 
 */
const groupByKey = (array, callBack) => {
    if (!Array.isArray(array)) {
        customLog(`传入参数【${array}】不是一个数组`)
        return
    }
    if (array.some(e => typeof e === 'string')) {
        customLog(`传入参数【${array}】含有单个字符串，不是一个完整对象数组`)
        return
    }
    // pre 默认是对象，根据回调函数key进行匹配分组，key需要从cur当前项获取并返回
    return array.reduce((pre, cur) => {
        const key = callBack(cur)
        if (!pre[key]) pre[key] = []
        pre[key] = [...pre[key], cur]
        return pre;
    }, {})
}

export {
    groupByKey
}

export default groupByKey;